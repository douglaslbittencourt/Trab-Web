/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Lucas
 */
@Entity
public class Anuncio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_anuncio")
    private Long id;

    @Column
    private String nome;

    @Column
    private String dataEntrada;

    @Column
    private String descricao;

    @Column
    private String servicos;

    @Column
    private double valor;

    @Column
    private int capacidade;

    @Column
    private String endereço;

    @Column
    private String cidade;

    @Column
    private double notaAnuncio;

    @Column
    private double qtdNotas;
    
    @Column
    private boolean alugado;

    @ManyToMany
    @JoinTable(name = "hospedes",
            joinColumns = {
                @JoinColumn(name = "id_anuncio")},
            inverseJoinColumns = {
                @JoinColumn(name = "hospede")})
    private List<Anuncio> hospede;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "hospedeiro", joinColumns = {
        @JoinColumn(name = "id_anuncio")},
            inverseJoinColumns = {
                @JoinColumn(name = "hospedeiro")})
    private List<Anuncio> hospedeiro;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    public Anuncio(Long id, String nome, String dataEntrada, String descricao, String servicos, double valor, int capacidade, String endereço, String cidade, double notaAnuncio, double qtdNotas, List<Anuncio> listaHospede, List<Anuncio> listHospedeiro, Usuario usuario, boolean alugado) {
        this.id = id;
        this.nome = nome;
        this.dataEntrada = dataEntrada;
        this.descricao = descricao;
        this.servicos = servicos;
        this.valor = valor;
        this.capacidade = capacidade;
        this.endereço = endereço;
        this.cidade = cidade;
        this.notaAnuncio = notaAnuncio;
        this.qtdNotas = qtdNotas;
        hospede = new ArrayList<>();
        hospedeiro = new ArrayList<>();
        this.usuario = usuario;
        this.alugado = alugado;
    }

    public Anuncio() {
        hospede = new ArrayList<>();
        hospedeiro = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(String dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getServicos() {
        return servicos;
    }

    public void setServicos(String servicos) {
        this.servicos = servicos;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public double getNotaAnuncio() {
        return notaAnuncio;
    }

    public void setNotaAnuncio(double notaAnuncio) {
        this.notaAnuncio = notaAnuncio;
    }

    public double getQtdNotas() {
        return qtdNotas;
    }

    public void setQtdNotas(double qtdNotas) {
        this.qtdNotas = qtdNotas;
    }

    public List<Anuncio> getHospede() {
        return hospede;
    }

    public void setHospede(List<Anuncio> hospede) {
        this.hospede = hospede;
    }

    public List<Anuncio> getHospedeiro() {
        return hospedeiro;
    }

    public void setHospedeiro(List<Anuncio> hospedeiro) {
        this.hospedeiro = hospedeiro;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public boolean isAlugado() {
        return alugado;
    }

    public void setAlugado(boolean alugado) {
        this.alugado = alugado;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Anuncio)) {
            return false;
        }
        Anuncio other = (Anuncio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Anuncio[ id=" + id + " ]";
    }

}
