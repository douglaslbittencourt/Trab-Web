/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Lucas
 */
@Entity
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_usuario")
    private Long id;

    @Column
    private String login;

    @Column
    private String senha;

    @Column
    private String nome;

    @Column
    private String data;

    @Column
    private String profissao;

    @Column
    private String telefone;

    @Column
    private double nota;

    @Column
    private double qtdnota;

    @OneToMany(mappedBy = "usuario")
    private List<Anuncio> listaAnuncio;

    @OneToMany(mappedBy = "usuario")
    private List<Anuncio> listaHospedagem;

    public Usuario(String login, String senha, String nome, String data, String profissao, String telefone, double nota, double qtdnota) {

        this.login = login;
        this.senha = senha;
        this.nome = nome;
        this.data = data;
        this.profissao = profissao;
        this.telefone = telefone;
        this.nota = nota;
        this.qtdnota = qtdnota;
        listaHospedagem = new ArrayList<>();
        listaAnuncio = new ArrayList<>();
    }

    public Usuario() {
        listaHospedagem = new ArrayList<>();
        listaAnuncio = new ArrayList<>();

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public double getQtdnota() {
        return qtdnota;
    }

    public void setQtdnota(double qtdnota) {
        this.qtdnota = qtdnota;
    }

    public List<Anuncio> getListaAnuncio() {
        return listaAnuncio;
    }

    public void setListaAnuncio(List<Anuncio> listaAnuncio) {
        this.listaAnuncio = listaAnuncio;
    }

//    public List<Anuncio> getListaHospedagem() {
//        return listaHospedagem;
//    }
//
//    public void setListaHospedagem(List<Anuncio> listaHospedagem) {
//        this.listaHospedagem = listaHospedagem;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Usuario[ id=" + id + " ]";
    }

}
