/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import down.AnuncioJpaController;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.swing.JOptionPane;
import modelo.Anuncio;
import modelo.Usuario;

/**
 *
 * @author 5105011523
 */
@ManagedBean(name = "anuncioBean")
@SessionScoped
public class AnuncioBean {

    private ArrayList<Usuario> listUsuarioA = new ArrayList<>();
    private ArrayList<Anuncio> listAnuncioA = new ArrayList<>();
    private Anuncio anuncio = new Anuncio();
    private String buscaCidade;
    private double buscaNota;
    private String buscaData;
    private DataModel<Anuncio> anuncioModel;
    private DataModel<Anuncio> anuncioModelPesquisa;
    List<Anuncio> pesquisa;

    public AnuncioBean() {
        anuncioModel = new ListDataModel<Anuncio>(listAnuncioA);
        anuncioModelPesquisa = new ListDataModel<Anuncio>(pesquisa);
    }

    public List<Anuncio> getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(List<Anuncio> pesquisa) {
        this.pesquisa = pesquisa;
    }

    public DataModel<Anuncio> getAnuncioModelPesquisa() {
        return anuncioModelPesquisa;
    }

    public void setAnuncioModelPesquisa(DataModel<Anuncio> anuncioModelPesquisa) {
        this.anuncioModelPesquisa = anuncioModelPesquisa;
    }

    public DataModel<Anuncio> getAnuncioModel() {
        return anuncioModel;
    }

    public void setAnuncioModel(DataModel<Anuncio> anuncioModel) {
        this.anuncioModel = anuncioModel;
    }

    public ArrayList<Usuario> getListUsuarioA() {
        return listUsuarioA;
    }

    public void setListUsuarioA(ArrayList<Usuario> listUsuarioA) {
        this.listUsuarioA = listUsuarioA;
    }

    public ArrayList<Anuncio> getListAnuncioA() {
        return listAnuncioA;
    }

    public void setListAnuncioA(ArrayList<Anuncio> listAnuncioA) {
        this.listAnuncioA = listAnuncioA;
    }

    public Anuncio getAnuncio() {
        return anuncio;
    }

    public void setAnuncio(Anuncio anuncio) {
        this.anuncio = anuncio;
    }

    public String getBuscaCidade() {
        return buscaCidade;
    }

    public void setBuscaCidade(String buscaCidade) {
        this.buscaCidade = buscaCidade;
    }

    public double getBuscaNota() {
        return buscaNota;
    }

    public void setBuscaNota(double buscaNota) {
        this.buscaNota = buscaNota;
    }

    public String getBuscaData() {
        return buscaData;
    }

    public void setBuscaData(String buscaData) {
        this.buscaData = buscaData;
    }

    public void salvar() {
        AnuncioJpaController ajc = new AnuncioJpaController();
        if (listAnuncioA.contains(anuncio)) {
            listAnuncioA.add(anuncio);
            try {
                ajc.edit(anuncio);
            } catch (Exception ex) {
                Logger.getLogger(AnuncioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "cadastrado " + anuncio.getNome());
        } else {
            listAnuncioA.add(anuncio);
            ajc.create(anuncio);
            JOptionPane.showMessageDialog(null, "cadastrado " + anuncio.getNome());
            anuncio = new Anuncio();
        }
        
    }

    public void buscaPorCidade() {
        AnuncioJpaController ajc = new AnuncioJpaController();
        JOptionPane.showMessageDialog(null, ajc.getAnuncioCount());
        pesquisa = ajc.listarAnuncioporCidade(buscaCidade);
        
    }

    public void buscaPorNota(AjaxBehaviorEvent event) {
        AnuncioJpaController ajc = new AnuncioJpaController();
        pesquisa = ajc.listarAnuncioporNota(buscaNota);
    }

    public void buscaPorData(AjaxBehaviorEvent event) {
        AnuncioJpaController ajc = new AnuncioJpaController();
        pesquisa = ajc.listarAnuncioporData(buscaData);
    }
}
