/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import down.AnuncioJpaController;
import down.UsuarioJpaController;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Anuncio;
import modelo.Usuario;

/**
 *
 * @author 5105011523
 */
@ManagedBean(name = "usuarioBean")
@SessionScoped
public class UsuarioBean {
    private ArrayList<Usuario> listUsuario = new ArrayList<>();
    private ArrayList<Anuncio> listAnuncio = new ArrayList<>();
    private Usuario usuario = new Usuario();

    public UsuarioBean() {
    }

    public ArrayList<Usuario> getListUsuario() {
        return listUsuario;
    }

    public void setListUsuario(ArrayList<Usuario> listUsuario) {
        this.listUsuario = listUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ArrayList<Anuncio> getListAnuncio() {
        return listAnuncio;
    }

    public void setListAnuncio(ArrayList<Anuncio> listAnuncio) {
        this.listAnuncio = listAnuncio;
    }
    
    public void salvar(){
         UsuarioJpaController ujc = new UsuarioJpaController();
        if(!listUsuario.contains(usuario)){
            listUsuario.add(usuario);
            ujc.create(usuario);
        }
        usuario = new Usuario();
        
    }
    
    public String retornaCadastro(){
        return "CadastroUsuario";
    }
    
    
}
