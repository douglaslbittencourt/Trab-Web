/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package down;

import down.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Anuncio;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import modelo.Usuario;

/**
 *
 * @author Lucas
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController() {
        emf = Persistence.createEntityManagerFactory("HospedagemPU");
    }
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) {
        if (usuario.getListaAnuncio() == null) {
            usuario.setListaAnuncio(new ArrayList<Anuncio>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Anuncio> attachedListaAnuncio = new ArrayList<Anuncio>();
            for (Anuncio listaAnuncioAnuncioToAttach : usuario.getListaAnuncio()) {
                listaAnuncioAnuncioToAttach = em.getReference(listaAnuncioAnuncioToAttach.getClass(), listaAnuncioAnuncioToAttach.getId());
                attachedListaAnuncio.add(listaAnuncioAnuncioToAttach);
            }
            usuario.setListaAnuncio(attachedListaAnuncio);
            em.persist(usuario);
            for (Anuncio listaAnuncioAnuncio : usuario.getListaAnuncio()) {
                Usuario oldUsuarioOfListaAnuncioAnuncio = listaAnuncioAnuncio.getUsuario();
                listaAnuncioAnuncio.setUsuario(usuario);
                listaAnuncioAnuncio = em.merge(listaAnuncioAnuncio);
                if (oldUsuarioOfListaAnuncioAnuncio != null) {
                    oldUsuarioOfListaAnuncioAnuncio.getListaAnuncio().remove(listaAnuncioAnuncio);
                    oldUsuarioOfListaAnuncioAnuncio = em.merge(oldUsuarioOfListaAnuncioAnuncio);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getId());
            List<Anuncio> listaAnuncioOld = persistentUsuario.getListaAnuncio();
            List<Anuncio> listaAnuncioNew = usuario.getListaAnuncio();
            List<Anuncio> attachedListaAnuncioNew = new ArrayList<Anuncio>();
            for (Anuncio listaAnuncioNewAnuncioToAttach : listaAnuncioNew) {
                listaAnuncioNewAnuncioToAttach = em.getReference(listaAnuncioNewAnuncioToAttach.getClass(), listaAnuncioNewAnuncioToAttach.getId());
                attachedListaAnuncioNew.add(listaAnuncioNewAnuncioToAttach);
            }
            listaAnuncioNew = attachedListaAnuncioNew;
            usuario.setListaAnuncio(listaAnuncioNew);
            usuario = em.merge(usuario);
            for (Anuncio listaAnuncioOldAnuncio : listaAnuncioOld) {
                if (!listaAnuncioNew.contains(listaAnuncioOldAnuncio)) {
                    listaAnuncioOldAnuncio.setUsuario(null);
                    listaAnuncioOldAnuncio = em.merge(listaAnuncioOldAnuncio);
                }
            }
            for (Anuncio listaAnuncioNewAnuncio : listaAnuncioNew) {
                if (!listaAnuncioOld.contains(listaAnuncioNewAnuncio)) {
                    Usuario oldUsuarioOfListaAnuncioNewAnuncio = listaAnuncioNewAnuncio.getUsuario();
                    listaAnuncioNewAnuncio.setUsuario(usuario);
                    listaAnuncioNewAnuncio = em.merge(listaAnuncioNewAnuncio);
                    if (oldUsuarioOfListaAnuncioNewAnuncio != null && !oldUsuarioOfListaAnuncioNewAnuncio.equals(usuario)) {
                        oldUsuarioOfListaAnuncioNewAnuncio.getListaAnuncio().remove(listaAnuncioNewAnuncio);
                        oldUsuarioOfListaAnuncioNewAnuncio = em.merge(oldUsuarioOfListaAnuncioNewAnuncio);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = usuario.getId();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<Anuncio> listaAnuncio = usuario.getListaAnuncio();
            for (Anuncio listaAnuncioAnuncio : listaAnuncio) {
                listaAnuncioAnuncio.setUsuario(null);
                listaAnuncioAnuncio = em.merge(listaAnuncioAnuncio);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Usuario> listarUsuario() {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("HospedagemPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("SELECT e FROM Usuario e");
            List<Usuario> disciplina = query.getResultList();
            return disciplina;
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
            return null;
        } finally {
            em.close();
        }
    }
    
}
