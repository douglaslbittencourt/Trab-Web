/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package down;

import controle.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Usuario;
import modelo.Anuncio;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Lucas
 */
public class AnuncioJpaController implements Serializable {

    public AnuncioJpaController() {
        emf = Persistence.createEntityManagerFactory("HospedagemPU");
    }
    private EntityManagerFactory emf;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Anuncio anuncio) {
        if (anuncio.getHospede() == null) {
            anuncio.setHospede(new ArrayList<Anuncio>());
        }
        if (anuncio.getHospedeiro() == null) {
            anuncio.setHospedeiro(new ArrayList<Anuncio>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario = anuncio.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                anuncio.setUsuario(usuario);
            }
            List<Anuncio> attachedHospede = new ArrayList<Anuncio>();
            for (Anuncio hospedeAnuncioToAttach : anuncio.getHospede()) {
                hospedeAnuncioToAttach = em.getReference(hospedeAnuncioToAttach.getClass(), hospedeAnuncioToAttach.getId());
                attachedHospede.add(hospedeAnuncioToAttach);
            }
            anuncio.setHospede(attachedHospede);
            List<Anuncio> attachedHospedeiro = new ArrayList<Anuncio>();
            for (Anuncio hospedeiroAnuncioToAttach : anuncio.getHospedeiro()) {
                hospedeiroAnuncioToAttach = em.getReference(hospedeiroAnuncioToAttach.getClass(), hospedeiroAnuncioToAttach.getId());
                attachedHospedeiro.add(hospedeiroAnuncioToAttach);
            }
            anuncio.setHospedeiro(attachedHospedeiro);
            em.persist(anuncio);
            if (usuario != null) {
                usuario.getListaAnuncio().add(anuncio);
                usuario = em.merge(usuario);
            }
            for (Anuncio hospedeAnuncio : anuncio.getHospede()) {
                hospedeAnuncio.getHospede().add(anuncio);
                hospedeAnuncio = em.merge(hospedeAnuncio);
            }
            for (Anuncio hospedeiroAnuncio : anuncio.getHospedeiro()) {
                hospedeiroAnuncio.getHospede().add(anuncio);
                hospedeiroAnuncio = em.merge(hospedeiroAnuncio);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Anuncio anuncio) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Anuncio persistentAnuncio = em.find(Anuncio.class, anuncio.getId());
            Usuario usuarioOld = persistentAnuncio.getUsuario();
            Usuario usuarioNew = anuncio.getUsuario();
            List<Anuncio> hospedeOld = persistentAnuncio.getHospede();
            List<Anuncio> hospedeNew = anuncio.getHospede();
            List<Anuncio> hospedeiroOld = persistentAnuncio.getHospedeiro();
            List<Anuncio> hospedeiroNew = anuncio.getHospedeiro();
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                anuncio.setUsuario(usuarioNew);
            }
            List<Anuncio> attachedHospedeNew = new ArrayList<Anuncio>();
            for (Anuncio hospedeNewAnuncioToAttach : hospedeNew) {
                hospedeNewAnuncioToAttach = em.getReference(hospedeNewAnuncioToAttach.getClass(), hospedeNewAnuncioToAttach.getId());
                attachedHospedeNew.add(hospedeNewAnuncioToAttach);
            }
            hospedeNew = attachedHospedeNew;
            anuncio.setHospede(hospedeNew);
            List<Anuncio> attachedHospedeiroNew = new ArrayList<Anuncio>();
            for (Anuncio hospedeiroNewAnuncioToAttach : hospedeiroNew) {
                hospedeiroNewAnuncioToAttach = em.getReference(hospedeiroNewAnuncioToAttach.getClass(), hospedeiroNewAnuncioToAttach.getId());
                attachedHospedeiroNew.add(hospedeiroNewAnuncioToAttach);
            }
            hospedeiroNew = attachedHospedeiroNew;
            anuncio.setHospedeiro(hospedeiroNew);
            anuncio = em.merge(anuncio);
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getListaAnuncio().remove(anuncio);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getListaAnuncio().add(anuncio);
                usuarioNew = em.merge(usuarioNew);
            }
            for (Anuncio hospedeOldAnuncio : hospedeOld) {
                if (!hospedeNew.contains(hospedeOldAnuncio)) {
                    hospedeOldAnuncio.getHospede().remove(anuncio);
                    hospedeOldAnuncio = em.merge(hospedeOldAnuncio);
                }
            }
            for (Anuncio hospedeNewAnuncio : hospedeNew) {
                if (!hospedeOld.contains(hospedeNewAnuncio)) {
                    hospedeNewAnuncio.getHospede().add(anuncio);
                    hospedeNewAnuncio = em.merge(hospedeNewAnuncio);
                }
            }
            for (Anuncio hospedeiroOldAnuncio : hospedeiroOld) {
                if (!hospedeiroNew.contains(hospedeiroOldAnuncio)) {
                    hospedeiroOldAnuncio.getHospede().remove(anuncio);
                    hospedeiroOldAnuncio = em.merge(hospedeiroOldAnuncio);
                }
            }
            for (Anuncio hospedeiroNewAnuncio : hospedeiroNew) {
                if (!hospedeiroOld.contains(hospedeiroNewAnuncio)) {
                    hospedeiroNewAnuncio.getHospede().add(anuncio);
                    hospedeiroNewAnuncio = em.merge(hospedeiroNewAnuncio);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = anuncio.getId();
                if (findAnuncio(id) == null) {
                    throw new NonexistentEntityException("The anuncio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Anuncio anuncio;
            try {
                anuncio = em.getReference(Anuncio.class, id);
                anuncio.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The anuncio with id " + id + " no longer exists.", enfe);
            }
            Usuario usuario = anuncio.getUsuario();
            if (usuario != null) {
                usuario.getListaAnuncio().remove(anuncio);
                usuario = em.merge(usuario);
            }
            List<Anuncio> hospede = anuncio.getHospede();
            for (Anuncio hospedeAnuncio : hospede) {
                hospedeAnuncio.getHospede().remove(anuncio);
                hospedeAnuncio = em.merge(hospedeAnuncio);
            }
            List<Anuncio> hospedeiro = anuncio.getHospedeiro();
            for (Anuncio hospedeiroAnuncio : hospedeiro) {
                hospedeiroAnuncio.getHospede().remove(anuncio);
                hospedeiroAnuncio = em.merge(hospedeiroAnuncio);
            }
            em.remove(anuncio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Anuncio> findAnuncioEntities() {
        return findAnuncioEntities(true, -1, -1);
    }

    public List<Anuncio> findAnuncioEntities(int maxResults, int firstResult) {
        return findAnuncioEntities(false, maxResults, firstResult);
    }

    private List<Anuncio> findAnuncioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Anuncio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Anuncio> listarAnuncioporCidade(String cidade) {
        String jpql = "select a from Anuncio a where a.cidade =:cidade_anuncio";
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospedagemPU");
        EntityManager em = emf.createEntityManager();
        List<Anuncio> resultado = em.createQuery(jpql, Anuncio.class).setParameter("cidade_anuncio", cidade).getResultList();
        if (resultado == null || resultado.isEmpty()) {
            return null;
        }
        return resultado;
    }
    
    public List<Anuncio> listarAnuncioporNota(double nota) {
        String jpql = "select a from Anuncio a where a.notaAnuncio =:notaAnuncio_anuncio";
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospedagemPU");
        EntityManager em = emf.createEntityManager();
        List<Anuncio> resultado = em.createQuery(jpql, Anuncio.class).setParameter("notaAnuncio_anuncio", nota).getResultList();
        if (resultado == null || resultado.isEmpty()) {
            return null;
        }
        return resultado;
    }
    
    public List<Anuncio> listarAnuncioporData(String data) {
        String jpql = "select a from Anuncio a where a.dataEntrada =:dataEntrada_anuncio";
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HospedagemPU");
        EntityManager em = emf.createEntityManager();
        List<Anuncio> resultado = em.createQuery(jpql, Anuncio.class).setParameter("dataEntrada_anuncio", data).getResultList();
        if (resultado == null || resultado.isEmpty()) {
            return null;
        }
        return resultado;
    }

    public Anuncio findAnuncio(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Anuncio.class, id);
        } finally {
            em.close();
        }
    }

    public int getAnuncioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Anuncio> rt = cq.from(Anuncio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
